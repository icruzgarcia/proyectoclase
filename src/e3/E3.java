package e3;

import java.util.Arrays;
import javax.swing.JOptionPane;

/**
 *
 * @author icruzgarcia
 * @since Version 22/01/2014
 */
public class E3 {

    public static void main(String[] args) {
        int opcion=0;
        int[] notas = new int[5];
        String[] alumnos = {"Pepe", "Manolo", "Juan", "Maria", "Ana"};
        Metodos.alumnado(notas);
        do{
       opcion=Integer.parseInt(JOptionPane.showInputDialog(null,"¿Que quieres hacer?\n"+"1-Ver nota más alta\n2-Ver el número de aprobados y suspensos"
                + "\n3-Ver la media\n4-Ver quien aprobo\n5-Ordenar los alumnos por nota\n6-Buscar un alumno"));
       switch (opcion){
       
           case 1:
               Metodos.alta(notas);
               break;
           case 2:
               Metodos.aproSus(notas);
               break;
           case 3:
               Metodos.media(notas);
               break;
           case 4:
               Metodos.aprobados(notas, alumnos);
               break;
           case 5:
               Metodos.orden(notas, alumnos);
               break;
           case 6:
               Metodos.pregunta(notas, alumnos);
               break;
           default:
               opcion=0;
       }
       }while(opcion!=0);
       }
}



