/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package e3;

import javax.swing.JOptionPane;

/**
 *
 * @author icruzgarcia
 */
public class Metodos {
//Encuentra el comentario oculto
    public static void alumnado(int[] notas) {
        int aprobado = 0, suspenso = 0;
        for (int i = 0; i < notas.length; i++) {
            int random = (int) (Math.random() * 10) + 1;
            notas[i] = random;//asignamos valores a las notas     
        }
    }

    public static void alta(int[] notas) {
        int alta=0;
        for (int i = 0; i < notas.length; i++) {
            if (notas[i] > alta) {
                alta = notas[i];
            }
        }
    }

    public static void aproSus(int[] notas) {
        int aprobado = 0, suspenso = 0;
        for (int i = 0; i < notas.length; i++) {
            int random = (int) (Math.random() * 10) + 1;
            notas[i] = random;//asignamos valores a las notas
            if (notas[i] >= 5) {//Con contadores auxiliares contamos el numero de aprobados y suspensos.
                aprobado++;
            } else if (notas[i] < 5) {
                suspenso++;
            }
        }
        JOptionPane.showMessageDialog(null, "Número de aprobados: " + aprobado + "\nNúmero de suspensos: " + suspenso);
    }

    public static void media(int[] notas) {
        int acumulador = 0;
        float media = 0.0F;
        for (int i = 0; i < notas.length; i++) {
            acumulador += notas[i];
            media = acumulador / notas.length;
        }
        JOptionPane.showMessageDialog(null, "Media de las notas: " + media);
    }

    public static  void pregunta(int[] notas, String[] alumnos) {
        String dialogo = JOptionPane.showInputDialog("Introduce el nombre del alumno");

        for (int j = 0; j < alumnos.length; j++) {
            if (alumnos[j].equalsIgnoreCase(dialogo)) {//equalsIgnoreCase no hace caso de mayusculas o minusculas.
                System.out.println(alumnos[j] + " tiene un " + notas[j]);//Esto visualizará la nota del alumno que nosotros pidamos
            }
        }

    }

    public static void aprobados(int[] notas, String[] alumnos) {
        for (int y = 0; y < alumnos.length; y++) {

            if (notas[y] >= 5) {
                System.out.println(alumnos[y] + "-->" + notas[y]);//Visualizará el nombre y notas de los alumnos aprobados.
            }


        }
    }

    public  static void orden(int[] notas, String[] alumnos) {
        System.out.println("Notas\tAlumnos");
        for (int i = 0; i < alumnos.length; i++) {
            int auxiliar = 0;
            String aux = "";
            for (int j = i + 1; j < alumnos.length; j++) {
                if (notas[i] > notas[j]) {
                    auxiliar = notas[i];
                    notas[i] = notas[j];
                    notas[j] = auxiliar;
                    aux = alumnos[i];
                    alumnos[i] = alumnos[j];
                    alumnos[j] = aux;
                }
            }
            System.out.println(alumnos[i] + "\t" + notas[i]);
        }


    }
}
